<?php 

namespace App\Models;

use CodeIgniter\Model;

class MemberTokenModel extends Model {
    protected $table = 'member_token';
    protected $allowedFields = ['id', 'member_id', 'auth_key'];
}
