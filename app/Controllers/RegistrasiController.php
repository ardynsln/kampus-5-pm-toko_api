<?php

namespace App\Controllers;

use App\Models\RegistrasiModel;
use CodeIgniter\RESTful\ResourceController;

class RegistrasiController extends RestfulController
{
    public function registrasi()
    {
        $validation = [
            'nama'      => 'required',
            'email'     => 'required',
            'password'  => 'required',
        ];
        if (!$this->validate($validation)) {
            return $this->responseHasil(
                403,
                false,
                $this->validator->getErrors()
            );
        }

        $data = [
            'nama'  => $this->request->getVar('nama'),
            'email' => $this->request->getVar('email'),
            'password' => password_hash($this->request->getVar('password'), PASSWORD_DEFAULT),
        ];

        $model = new RegistrasiModel();
        $model->save($data);
        return $this->responseHasil(200, true, $data);
    }
}
