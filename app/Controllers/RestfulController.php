<?php

namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;

class RestfulController extends ResourceController
{

    protected $format = "json";

    public function index()
    {
        return view('welcome_message');
    }

    protected function responseHasil($code, $status, $data)
    {
        return $this->respond([
            'code'      => $code,
            'status'    => $status,
            'data'      => $data,
        ]);
    }
}
