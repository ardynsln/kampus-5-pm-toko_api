<?php

namespace App\Controllers;

use App\Models\MemberModel;
use App\Models\MemberTokenModel;
use App\Models\RegistrasiModel;
use Firebase\JWT\JWT;

class LoginController extends RestfulController
{

    public function login()
    {
        $validation = [
            'email'     => 'required',
            'password'  => 'required',
        ];
        if (!$this->validate($validation)) {
            return $this->responseHasil(
                403,
                false,
                $this->validator->getErrors()
            );
        }

        $data = [
            'email'     => $this->request->getVar('email'),
            'password'  => $this->request->getVar('password'),
        ];

        $member = new MemberModel();
        $user = $member->where('email', $data['email'])->first();

        if (!$user) {
            return $this->responseHasil(400, false, 'Email tidak terdaftar');
        }

        if (!password_verify($data['password'], $user['password'])) {
            return $this->responseHasil(400, false, "Password tidak valid");
        }

        // unset
        unset($user['password']);

        $key        = getenv('TOKEN_SECRET');
        $payload    = (array)$user;
        $encode     = JWT::encode($payload, $key, 'HS256');

        $login = new MemberTokenModel();
        $login->save([
            'member_id' => $user['id'],
            'auth_key'  => $encode,
        ]);

        return $this->respond([
            'status'    => true,
            'code'      => 200,
            'message'   => 'success',
            'data'      => [
                'user'          => $user,
                'jwt_token'     => $encode,
            ],
        ]);
    }


    private function randomString($length = 100)
    {
        $karakkter = '012345678dssd9abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $panjang_karakter = strlen($karakkter);
        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $str .= $karakkter[rand(0, $panjang_karakter - 1)];
        }
        return $str;
    }
}
