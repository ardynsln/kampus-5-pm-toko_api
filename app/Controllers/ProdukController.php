<?php

namespace App\Controllers;

use App\Models\ProdukModel;

class ProdukController extends RestfulController
{

    public function index()
    {
        $produkModel = new ProdukModel();
        $produks = $produkModel->findAll();
        return $this->responseHasil(200, true, $produks);
    }


    public function create()
    {
        $validation = [
            'kode_produk'   => 'required',
            'nama_produk'   => 'required',
            'harga'         => 'required|numeric',
        ];
        if (!$this->validate($validation)) {
            return $this->responseHasil(
                403,
                false,
                $this->validator->getErrors()
            );
        }

        $data = [
            'kode_produk'   => $this->request->getVar('kode_produk'),
            'nama_produk'   => $this->request->getVar('nama_produk'),
            'harga'         => $this->request->getVar('harga'),
        ];
        $model =  new ProdukModel();
        $model->insert($data);
        return $this->responseHasil(200, true, $data);
    }


    public function produk($id)
    {
        $model = new ProdukModel();
        $produk = $model->find($id);
        return $this->responseHasil(200, true, $produk);
    }


    public function ubah()
    {
        $validation = [
            'produk_id'     => 'required|numeric',
            'kode_produk'   => 'required',
            'nama_produk'   => 'required',
            'harga'         => 'required|numeric',
        ];
        if (!$this->validate($validation)) {
            return $this->responseHasil(
                403,
                false,
                $this->validator->getErrors()
            );
        }

        $data = [
            'kode_produk'   => $this->request->getVar('kode_produk'),
            'nama_produk'   => $this->request->getVar('nama_produk'),
            'harga'         => $this->request->getVar('harga'),
        ];
        
        $model =  new ProdukModel();
        $model->update(
            $this->request->getVar('produk_id'),
            $data
        );
        return $this->responseHasil(200, true, $data);
    }


    public function hapus($id)
    {
        $model = new ProdukModel();
        $produk = $model->delete($id);
        return $this->responseHasil(200, true, $produk);
    }
}
